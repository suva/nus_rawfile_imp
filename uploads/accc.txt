transport cost:

The expenses involved in moving products or assets to a different place, 
which are often passed on to consumers. For example, a business would
 generally incur a transportation cost if it needs to bring 
its products to retailers in order to have them offered for sale to consumers.

Accumulated dep:
Accumulated Depreciation is a long-term contra asset account (an asset account with
 a credit balance) that is reported 
on the balance sheet under the heading Property, Plant, and Equipment.

Long term liability:

Long-term liabilities, in accounting, form part of a section of the balance sheet that lists
 liabilities not due within the next 12 months including debentures, loans, deferred tax liabilities
 and pension obligations.


Financial stmnt :
Financial statements (or financial report) is a formal record of the financial activities
 and position of a business, person, or other entity.

C V P:
Image result for cost volume profit analysis definition
Cost-volume-profit (CVP) analysis is used to determine how changes
 in costs and volume affect a company's operating income and net income.

Capital budgting :
Capital budgeting (also known as investment appraisal) is the process by which a 
company determines whether projects (such as investing in R&D, opening a new branch, 
replacing a machine) are worth pursuing. 

Work in process :
Work in process (WIP), work in progress (WIP), goods in process, or in-process inventory are
 a company's partially finished goods waiting for completion and eventual sale or the value of these items

Break evn point:
In economics and business, specifically cost accounting, the break-even point (BEP) is the point at
 which cost or expenses and revenue are equal: there is no net loss or gain, and one has "broken even.". 

Accural basis accounting :

accrual basis of accounting definition. The accounting method under which revenues are recognized on
 the income statement when they are earned (rather than when the cash is received).



Conversion costs :
Conversion costs are the combination of direct labor costs plus manufacturing overhead costs

purchase invoice:
A commercial document or bill presented to a buyer by a seller or service provider for 
payment within a stated time frame that indicates what has been purchased, in what amount and for what price.

sales returns and allowances :
A contra revenue account that reports 1) merchandise returned by a customer, and
 2) the allowances granted to a customer because the seller shipped improper or defective merchandise.

Accounting:
the process or work of keeping financial accounts.

A.T.B:
An adjusted trial balance is a listing of all the account titles and 
balances contained in the general ledger after the adjusting entries for an accounting 
period have been posted to the accounts.
























